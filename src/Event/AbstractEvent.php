<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Event;

use App\Connection\SSHConnection;
use App\Core\Config;
use App\Core\App;
use App\Factory\CommandFactory;

abstract class AbstractEvent implements EventInterface
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    abstract protected function getEndpoint(): string;

    abstract protected function getEventName(): string;

    /**
     * @throws \Exception
     */
    public function dispatch()
    {
        $servers = $this->getServersForEndpoint();
        $commands = $this->getCommands();

        if (count($commands)) {
            if (count($servers)) {
                $output = '';
                foreach ($servers as $server) {
                    $this->validate($server);
                    foreach ($commands as $commandName) {
                        $command = CommandFactory::getCommand($commandName, $server, $this->getBranch(), $this->data);
                        $command->execute($output);
                    }
                }
            }
        }else {
            throw new \Exception('Commands for ' . $this->getEventName() . ' not found!', 500);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getServersForEndpoint(): array {
        $servers = [];
        $configEndpoints = Config::get('endpoints');
        $branch = $this->getEndpoint();
        $cnfServers = Config::get('servers');

        if (isset($configEndpoints[$this->getEventName()][$branch]) && count($configEndpoints[$this->getEventName()][$branch])) {
            foreach ($configEndpoints[$this->getEventName()][$branch] as $serverName) {
                $server = isset($cnfServers[$serverName]) ? $cnfServers[$serverName] : null;
                if ($server && is_array($server) && $this->checkServer($server)) {
                    try {
                        $sshConnection = new SSHConnection($server['host'], $server['port'], $server['user'], $server['pass']);
                        $server['conn'] = $sshConnection;
                        $servers[] = $server;
                    }catch (\Exception $exception) {
                        App::log('Server ' . $serverName . ': ' . $exception->getMessage());
                    }
                }
            }
        }

        return $servers;
    }

    private function checkServer($server): bool {
        foreach (['host', 'port', 'user', 'pass'] as $field) {
            if (!isset($server[$field]) || $server[$field] == '') {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getCommands(): array
    {
        $commands = Config::get('commands');
        return isset($commands[$this->getEventName()]) ? $commands[$this->getEventName()] : [];
    }

    protected function getBranch(): string {
        return 'origin/' . $this->getEndpoint();
    }

    /**
     * @param $server
     * @throws \Exception
     */
    protected function validate($server) {
        $output = '';
        foreach (['GitFetch', 'GitLogCommit'] as $commandName) {
            $output = '';
            $command = CommandFactory::getCommand($commandName, $server, $this->getBranch(), $this->data);
            $command->execute($output, false);
        }

        try {
            $this->validateRepositoryStatus($output);
        }catch (\Exception $exception) {
            throw new \Exception($server['dir'] . ': ' . $exception->getMessage(), 500);
        }
    }

    /**
     * @param $log
     * @throws \Exception
     */
    protected function validateRepositoryStatus($log)
    {
        if (!preg_match('/' . str_replace('/', '\/', $this->getBranch()) . '[,)]/', $log)) {
            throw new \Exception('Commit was not found with branch \'' . $this->getBranch() . '\' hash:'.$this->data['checkout_sha'].' log:'.$log);
        }
    }
}