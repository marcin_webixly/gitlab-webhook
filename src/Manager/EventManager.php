<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Manager;

use App\Event\EventInterface;

class EventManager
{
    const EVENT_NAME_FORMAT = '\App\Event\%sEvent';

    private $request;

    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * @return EventInterface
     * @throws \Exception
     */
    private function getEvent(): EventInterface {
        $eventName = $this->nameToCamelCase($this->request['event_name']);
        $eventClass = sprintf(self::EVENT_NAME_FORMAT, $eventName);

        if (!class_exists($eventClass)) {
            throw new \Exception('Event class ' . $eventClass . ' not exists!', 500);
        }

        return new $eventClass($this->request);
    }

    private function nameToCamelCase($name): string {
        return implode('', array_map(function ($value) {
            return ucfirst($value);
        }, explode('_', $name)));
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $event = $this->getEvent();
        $event->dispatch();
    }
}