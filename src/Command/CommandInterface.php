<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Command;

interface CommandInterface
{
    public function __construct($server, $branch, $data);

    /**
     * @param $output
     * @param bool $raw
     * @return void
     */
    public function execute(&$output, $raw = true);

    public function getCommand(): string;
}