<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Command;

class GitResetCommand extends AbstractCommand
{
    public function getCommand(): string
    {
        return 'git reset ' . $this->branch .' && git clean -df';
    }
}