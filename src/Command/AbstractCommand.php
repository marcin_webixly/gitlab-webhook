<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Command;

abstract class AbstractCommand implements CommandInterface
{
    protected $server;
    protected $branch;
    protected $data;

    public function __construct($server, $branch, $data) {
        $this->server = $server;
        $this->branch = $branch;
        $this->data = $data;
    }

    public function execute(&$output, $raw = true) {
        $command = $this->getCommandBase() . $this->getCommand() . " 2>&1";
        $ssh = $this->server['conn']->getSSH();
        $out = $ssh->exec($command);
        if ($raw) {
            $output .= "$command\n";
            $output .= "-------------------- OUTPUT ---------------------\n";
            $output .= $out . "\n";
            $output .= "-------------------------------------------------\n\n\n";
        }else {
            $output = str_replace("\n", "", $out);
        }
    }

    private function getCommandBase() {
        return 'cd ' . $this->server['dir'] . ' && ';
    }
}