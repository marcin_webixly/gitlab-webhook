<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

namespace App\Core;

use App\Manager\EventManager;

class App
{
    private $data = [];

    /**
     * App constructor.
     * @throws \Exception
     */
    public function __construct() {
        $this->validateRequestMethod();
        $this->validateToken();
        $this->handleRequestBody();
        $this->checkProject();
    }

    /**
     * @throws \Exception
     */
    private function validateRequestMethod() {
        if (strtolower($_SERVER['REQUEST_METHOD']) !== 'post') {
            throw new \Exception('Invalid request method!', 500);
        }
    }

    /**
     * @throws \Exception
     */
    private function validateToken() {
        $token = Config::get('misc')['gitlab_token'];
        $gitlabToken = (isset($_SERVER['HTTP_X_GITLAB_TOKEN'])) ? $_SERVER['HTTP_X_GITLAB_TOKEN'] : null;

        if ($token !== $gitlabToken) {
            throw new \Exception('Invalid token', 500);
        }
    }

    /**
     * @throws \Exception
     */
    private function handleRequestBody() {
        $inputData = json_decode(file_get_contents('php://input'), true);
        if (!is_array($inputData)) {
            throw new \Exception('Invalid request!', 500);
        }
        $this->logData($inputData);
        $this->data = $inputData;
    }

    /**
     * @throws \Exception
     */
    private function checkProject() {
        $allowedProjects = Config::get('projects');
        $projectNameFromData = isset($this->data['project']['name']) ? $this->data['project']['name'] : null;

        if (!$projectNameFromData) {
            throw new \Exception('No project name in request', 500);
        }

        if (!in_array($projectNameFromData, $allowedProjects)) {
            throw new \Exception('Request for not allowed project ' . $projectNameFromData, 500);
        }
    }

    /**
     * @throws \Exception
     */
    public function listen() {
        $eventManager = new EventManager($this->data);
        $eventManager->run();
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    private function logData(array $data) {
        file_put_contents(Config::get('misc')['log_dir'] . DIRECTORY_SEPARATOR . time() . '.txt', json_encode($data));
    }

    /**
     * @param $msg
     */
    public static function log($msg) {
        if ($msg != '') {
            file_put_contents('var' . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR .  'main.log', '[' . date('Y-m-d H:i:s') . '] ' . $msg . "\n", FILE_APPEND);
        }
    }
}