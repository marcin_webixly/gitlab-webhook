<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

use App\Core\App;
use App\Core\Config;

chdir(dirname(__DIR__));
require dirname(__DIR__).'/vendor/autoload.php';

try {
    Config::load();
    $app = new App();
    $app->listen();
    $msg = 'Update finished successfully';
}catch (Exception $exception) {
    $msg = $exception->getMessage();
    echo $msg;
}

App::log($msg);
