<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

return [
    'exampleProjectName' => [
        'endpoints' => [
            'push' =>  [
                // Push into exampleBranch to launch synchronization on exampleServer
                'exampleBranch' => ['exampleServer'],
                //'test' => ['exampleTestServer'],
                //'dev' => ['exampleDevServer', 'exampleDevServer2'],
            ],
            'tag_push' => [
                //'prod' => ['exampleProdServer']
            ]
        ]
    ]
];