<?php
/**
 * Vertiso (https://vertiso.pl)
 *
 * @copyright Copyright (c) 2019 Vertiso (https://vertiso.pl)
 * @author    Marcin Zagórski <vertiso851@gmail.com>
 */

return [
    'config' => [
        'project_var' => 'project',
        'log_file' => getcwd() . '/var/log/log.txt',
    ]
];